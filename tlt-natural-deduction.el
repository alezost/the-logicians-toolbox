(defcustom tlt-nd-stars '(1 "Sterne")
  "Number and name of the star column.")

(defcustom tlt-nd-rowcolumn '(2 "Zeile")
  "Number and name of the column to be numbered.")

(defcustom tlt-nd-formulas '(3 "Formel")
  "Number and name of the formula column.")

(defcustom tlt-nd-reference '(4 "Bezug")
  "Number and name of the reference column.")

(defcustom tlt-nd-rule '(5 "Regel")
  "Number and name of the rule column.")

(defcustom tlt-nd-hyp "Hyp"
  "Name of the assertion tlt-nd-rule.")

(defcustom tlt-nd-capitalize t
  "Whether to capitalize rule names automatically when aligning the table.")

(defcustom tlt-nd-no-ref-rules '(tlt-nd-no-stars tlt-nd-new-star)
  "Functions of rules which do not reference any line.")

(defcustom tlt-nd-rule-hyp '(:rule hyp
                                   :names ("Hyp" "$Hyp$" "Hypothesis")
                                   :refs 0
                                   :star-function tlt-nd-new-star)
  "Rule for introducing a hypothesis.")

(defcustom tlt-nd-rule-intro-and '(:rule intro-and
                                         :names ("$I\\land$" "I&")
                                         :refs 2
                                         :star-function tlt-nd-unite-from-reflist)
  "Rule for introducing a conjunction.")

(defcustom tlt-nd-rule-intro-or '(:rule intro-or
                                        :names ("$I\\lor$" "Iv")
                                        :refs 1
                                        :star-function tlt-nd-adopt-stars)
  "Rule for introducing a disjunction.")

(defcustom tlt-nd-rule-elim-if '(:rule elim-if
                                       :names ("$E\\to$" "E->")
                                       :refs 2
                                       :star-function tlt-nd-unite-from-reflist)
  "Rule for eliminating a conditional.")

(defcustom tlt-nd-rule-intro-if '(:rule intro-if
                                        :names ("$I\\to$" "I->")
                                        :refs 2
                                        :star-function tlt-nd-subtract-from-reflist)
  "Rule for eliminating a conditional.")

(defcustom tlt-nd-rule-theorem '(:rule special-proven
                                       :names ("AL" "PC" "" "PL" "S1" "S2" "S3" "S4" "S5")
                                       :refs 2
                                       :star-function tlt-nd-no-stars)
  "Rule for introducing a formula already proved.")

(defcustom tlt-nd-rule-elim-if-multi '(:rule elim-if-multi
                                             :names ("$I\\to_+$" "$I\\to_{+}$" "$I\\to^+$" "$I\\to^{+}$" "I->+")
                                             :refs n
                                             :star-function tlt-nd-subtract-from-reflist)
  "Rule for eliminating several hypotheses at once.")

(defcustom tlt-nd-rule-elim-negation '(:rule elim-if-negation
                                             :names ("$E\\lnot$")
                                             :refs 2
                                             :star-function tlt-nd-unite-from-reflist)
  "Rule for introducing the bottom sign '⊥'.")

(defcustom tlt-nd-rule-intro-negation '(:rule elim-if-negation
                                              :names ("$I\\lnot$" "$I-$" "I~")
                                              :refs 2
                                              :star-function tlt-nd-subtract-from-reflist)
  "Rule for introducing a negation.")

(defcustom tlt-nd-proof-rules '(tlt-nd-rule-hyp
                                tlt-nd-rule-intro-and
                                tlt-nd-rule-intro-or
                                tlt-nd-rule-intro-if
                                tlt-nd-rule-elim-if
                                tlt-nd-rule-theorem
                                tlt-nd-rule-elim-if-multi
                                tlt-nd-rule-intro-negation
                                tlt-nd-rule-elim-negation)
  "List of rules to be considered for natural deduction proofs.")

(defvar tlt-nd-table nil
  "The table last fetched by `tlt-nd-get-table', possibly changed by functions like `tlt-nd-apply-rules' or `tlt-nd-correct.")

(defun tlt-nd-capitalize-first-char (string)
  "Return STRING, first char capitalized.
        Return zero-length strings as-is."
  (if (equal string "") ""
    (concat (upcase (substring string 0 1)) (substring string 1))))

(defun tlt-nd-next-field-until (end)
  "From the position of point onwards until position END,
search for the next table field (which is possibly in the next row).
If it exists until END, go there and return `t'.
If it does not exist, return `nil'.
Great parts copied from `org-table-next-field'."
  (let ((org-table-tab-jumps-over-hlines t))
    (condition-case nil
        (progn
          (re-search-forward "|" end)                        ; go to the next occurrence of "|"
          (if (looking-at "[ \t]*$")                         ; if we are at the end of a line,
              (re-search-forward "|" end))                   ; go to the next occurrence of "|" (in the next line)
          (if (looking-at "-")                               ; if it is a horizontal line
              (re-search-forward "^[ \t]*|\\([^-]\\)" end t) ; go inside the first field of the next row;
            (goto-char (match-beginning 1))))                ; and move to the beginning of the field
      (error (if (equal end (point))
                 nil t)))))

(defun tlt-nd-next-field ()
  "Go to the next field. Return nil if at the end of the table."
  (tlt-nd-next-field-until (1- (org-table-end))))

(defun tlt-nd-get-row ()
  "Return a list of the contents of every cell in the row at hand.
    The strings are trimmed, capitalized and without properties.
      If `tlt-nd-capitalize' is non-nil, capitalize the first character of each column name."
  (let ((rowend (re-search-forward "|\s*$" nil t))
        (org-table-tab-jumps-over-hlines t)
        (search t)
        (row nil))
    ;; If not at the beginning of line, go there ;;
    (unless (bolp)
      (beginning-of-line))
    ;; move point right of the first occurrence of "|" ;;
    (re-search-forward "|")
    ;; get the field at hand and push it to `row'. Repeat until the end of the row ;;
    (while search
      (looking-at "[^|\r\n]*")

      (let* ((field-trim
              (org-trim                         ; trim whitespaces
               (buffer-substring-no-properties  ; and remove properties
                (match-beginning 0)             ; from the beginning of the field
                (match-end 0))))                ; to the end of the field

             (field
              ;; capitalize depending on custom variable ;;
              (if tlt-nd-capitalize
                  (tlt-nd-capitalize-first-char field-trim)
                field-trim)))

        (push field row))

      (setq search (tlt-nd-next-field-until rowend)))
    ;; return row ;;
    (reverse row)))

(defun tlt-nd-get-table ()
  "Store the table at hand in `tlt-nd-table'."
  (let ((end (org-table-end))
        (search t)
        (table nil))
    (save-excursion
      (goto-char (org-table-begin))
      ;; Loop
      (while search
        (push
         (tlt-nd-get-row)
         table)
        (setq search (tlt-nd-next-field)))
      ;; return table
      (setq tlt-nd-table (reverse table)))))

(defun tlt-nd-line (line)
  "Get list for line number LINE stored in `tlt-nd-table'.
Set the variable if it is nil."
  (if tlt-nd-table
      (nth (1- line) tlt-nd-table)
    (nth (1- line) (tlt-nd-get-table))))

(defun tlt-nd-row (row)
  "Get row number ROW from the table stored in `tlt-nd-table'.
     If there is no table stored, create it from the table at hand."
  (if tlt-nd-table
      (nth row tlt-nd-table) ; not -1 to ignore header
    (nth row (tlt-nd-get-table))))

(defun tlt-nd-column (colnum)
  "Get column number COLNUM."
  (unless tlt-nd-table (tlt-nd-get-table))
  (let ((counter 1)
        (length (length tlt-nd-table))
        (col nil))
    (while (<= counter length)
      (push
       (nth (1- colnum)
            (tlt-nd-line counter))
       col)
      (setq counter (1+ counter)))
    (reverse col)))

(defun tlt-nd-column-headless (colnum)
  "Get column number COLNUM without headlines."
  (cdr (tlt-nd-column colnum)))

(defun tlt-nd-field (colnum row)
  "Get field in column COLNUM and row ROW."
  (nth (1- colnum) (tlt-nd-row row)))

(defun tlt-nd-set-field (colnum row value)
  "Set field in column COLNUM and row ROW of `tlt-nd-table' to VALUE."
  (setf  (nth (1- colnum) (tlt-nd-row row)) value))

(defun replace-string-in-list (old new list)
  "Return the result of replacing OLD item by NEW item in LIST.

        Example:
        'old' 'new', '(1 'old' 2) → '(1, 'new' 2) "
  (let ((outlist))
    (dolist (element list outlist)
      (if (equal element old)
          (push new outlist)
        (push element outlist)))
    (reverse outlist)))

(defun order-list-string-int (list)
  "Return the result of ordering LIST by the numbers which are its members (stored as strings).

    Examples:
  '('3' '2' '1')             → '('1' '2' '3')
  '('2' '1' '5' '3' '2' '1') → '('1' '1' '2' '2' '3' '5')"
  (let* ((unquoted (mapcar #'string-to-number list)) ; important: (string-to-number "m3") returns 0!
         (sorted (sort unquoted #'<=)))
    (mapcar #'int-to-string sorted)))

(defun order-string (string)
  "RETURN the result of ordering the numbers occurring in STRING, which is of the form 'num_1,num_2, ...,num_n', as a string.
        Examples:
        '3,2,1'     → '1,2,3'
        '1,5,2,1,3' → '1,1,2,3,5'"
  (let* ((clean (gnus-strip-whitespace                     ; raw numstring
                 (substring-no-properties string)))                 ; with properties removed
         (list (split-string clean ","))
         (sorted (order-list-string-int list))
         (sorted-string (string-join sorted ",")))
    (if (equal sorted-string "0") "-"
      sorted-string)))

(defun adjust-numbers ()
  "Adjust numbers."
  (let* ((ref (car tlt-nd-reference))                                    ; 4 aus (4 "Bezug")
         (row (car tlt-nd-rowcolumn))                                    ; 2 aus (2 "Zeile")
         (lines (length tlt-nd-table))                                   ; number of lines +1
         (org-table-allow-automatic-line-recalculation nil)              ; done with `ctrl-c-ctrl-c-hook' later anyway
         (org-table-automatic-realign nil)
         (refs (tlt-nd-column-headless ref))                             ; reference column
         (rownum 1))                                                     ; the correct row number (serves as counter, too!)

    (while (< rownum lines)
      (let* ((rownum-str (int-to-string rownum))                         ; 1 → "1"
             (actnum (tlt-nd-field row rownum))                          ; the actual (possibly wrong!) row number as a single-number string
             (counter2 1))                                               ; second counter to loop over the reference column

        ;; Getting the ROW NUMBERS right;;

        (unless (equal rownum-str actnum)                                ; if the correct rownumber rownum-str is not the actual rownumber actnum,
          (tlt-nd-set-field row rownum rownum-str)                       ; replace the actual (wrong!) row number with the right one

         ;;;             Getting the REFERENCE NUMBERS right             ;;;

          ;; Now that we have spotted a wrong row number and corrected   ;;
          ;; it in the row number column, we also need to correct it in  ;;
          ;; the reference column. Thus, we loop over the reference      ;;
          ;; column and replace every occurrence of the wrong row number ;;
          ;; by the right row number marked with "m".                    ;;

          ;; The "m" makes sure that if this correct reference number    ;;
          ;; is at a wrong place in the row number column, our function  ;;
          ;; won't "correct" our right number. We basically say          ;;
          ;; "this is the correct number, don't change it!"              ;;

          ;; Note that the unless-clause is not finished!                ;;

          (while (< counter2 lines)
            (let* ((numstring (nth (1- counter2) refs))                  ; the nth field of the rownumber column, a number string
                   (refnums (split-string numstring ","))                ; list of references in field, e.g. ("REF1" "REF2" "REF3")

                   (wrongnum actnum)                                     ; wrongnum is the actual row number in string form (under the above condition)
                   (rightnum rownum-str)                                 ; rightnum is the correct row number in string-form
                   (rightnum-marked (concat "m" rightnum))               ; rightnum-marked is the right number marked with "m", e.g. "m2"

                   ;; Now we replace our wrong rumber in the the reference list with ;;
                   ;; the marked right number and conert it back to a string         ;;

                   (refnums-right-list
                    (replace-string-in-list wrongnum rightnum-marked refnums))      ; replace all occurrences of the wrongnum in refnums by the marked rightnum

                   (refnums-right-str (string-join refnums-right-list "," )))       ; and make a string out of it

              (unless (or                                               ; unless the ref field is empty (else the ref field would get the row number of the empty field)
                       (equal numstring "")                             ; that is the empty string
                       (equal numstring "-"))                           ; or there is a hyphen in it "-",
                (tlt-nd-set-field ref counter2 refnums-right-str))      ; put the new string back in the field

              ;; In any case, whether we changed the reference field or not ;;
              ;; increment the counter by one to do the same thing for the  ;;
              ;; next reference field.                                      ;;

              (setq counter2 (1+ counter2)))))

        ;; Now we have updated the refernce fields with the correct number of   ;;
        ;; the FIRST row. So we repeat the while-loop to do this for EVERY row! ;;

        (setq rownum (1+ rownum)))

      ;; Now that every row number is correct and the row numbers in the reference column are adjusted, ;;
      ;; we remove our marks and sort the reference field so that the numbers are in the correct order  ;;

      (let ((counter3 2))                                                   ; create a third and last counter
        (while (< counter3 lines)                                           ; and loop over every row
          (let* ((marked (tlt-nd-field ref counter3))

                 (unmarked (replace-regexp-in-string "m" "" marked))
                 (sorted (order-string unmarked))
                 (forms (car tlt-nd-formulas)))

            (unless (equal (org-table-get counter3 forms) "")  ; unless the formula cell is empty
              (tlt-nd-set-field ref counter3 sorted)))  ; put the new number in there

          (setq counter3 (1+ counter3)))))))

(defun tlt-nd-nth-char (n string)
  "Return Nth char in STRING. Nth is one-based."
  (substring string (1- n) n))

(defun delete-star (star str)
  "Replace '+' at position STAR in STR by '-'.
Assumes that at position STAR, there actually is a '+'."
  ;; replace the star by a "-"
  (replace-regexp-in-string  ; replace any "+" at level star-level-act
   (format "\\([\\+\\|\\-]\\{%d\\}\\)\\(\\+\\)\\([\\+\\|\\-]*\\)" (1- star)) ; 1- because we exclude them so the first char is the star level

   ;;  first group: all occurrences of "+" or "-" until, excluding, `right-star'
   ;;  second group: exactly one occurrence of "+" or "-": the one to replace  ;;
   ;;  third group: zero or more occurrences of "+" or "-"  - anything after              ;;
   "\\1-\\3" ; the wrong star is replaced by a "-"
   ;;  first group, second group replaced by ";%d;", third group          ;;
   str))

(defun add-star (no-star str)
  "Replace '-' at position NO-STAR in STR by '+'.
Assumes that at position NO-STAR, there actually is a '-'."
  (replace-regexp-in-string  ; replace any "+" at level star-level-act by ";star-amount;" if it exists
   (format "\\([\\+\\-]\\{%d\\}\\)\\([\\+\\|\\-]\\)\\([\\+\\|\\-]*\\)" (1- no-star)) ; 1- because we exclude them so the first char is the star level

   ;;  first group: all occurrences of "+" or "-" until, excluding, `no-star' - anything before
   ;;  second group: exactly one occurrence of "+" or "-": the one to replace  ;;
   ;;  third group: zero or more occurrences of "+" or "-"  - anything after              ;;
   "\\1+\\3" ; the wrong star is replaced by a "-"
   ;;  first group, second group replaced by ";%d;", third group          ;;
   str))

(defun ensure-no-star (pos str)
  "Ensure that at position POS in string STR, there is a '-', and return the resulting string."
  (if (not (equal (tlt-nd-nth-char pos str) "-"))
      (delete-star pos str)
    str))

(defun ensure-star (pos str)
  "Ensure that at position POS in string STR, there is a '+', and return the resulting string."
  (if (not (equal (tlt-nd-nth-char pos str) "+"))
      (add-star pos str)
    str))

(defun tlt-nd-count-hyps-until (rownumber)
  "Return the number of hypotheses in `tlt-nd-table' until (excluding) ROWNUMBER."
  (let ((hypcount 0)
        (counter 1))
    (while (< counter rownumber)
      (let* ((row (tlt-nd-row counter))
             (rulecol (1- (car tlt-nd-rule)))
             (rule (string-trim-left (nth rulecol row)))
             (hypnames (plist-get tlt-nd-rule-hyp :names)))
        (when (member rule hypnames)
          (setq hypcount (1+ hypcount)))
        (setq counter (1+ counter))))
    hypcount))

(defun tlt-nd-count-hyps ()
  "Return the number of hypotheses in `tlt-nd-table'."
  (tlt-nd-count-hyps-until (1- (length tlt-nd-table))))

(defun tlt-nd-fill-star (number-of-hyps star-string)
  "Fill the STAR-STRING with occurrences of '-' until the number of ocurrences of '+' or '-' is NUMBER-OF-HYPS.
        If NUMBER-OF-HYPS is the same length as STAR-STRING, just return the STAR-STRING.
  Examples:
  number-of-hyps 4, star-string '-+' → '-+--'.
  number-of-hyps 2, star-string '-++' → '-++'."
  (let* ((length (length star-string))
         (diff (- number-of-hyps length)))
    (if (> diff 0)
        (concat star-string
                (make-string diff ?-))
      star-string)))

(defun tlt-nd-fill-stars (number-of-hyps starlist)
  "Fill all star-strings which are element of STARLIST with occurrences of '-'
      until the number of occurrences of '+' or '-' is that of NUMBER-OF-HYPS.
     Return a list of the filled star-strings."
  (let ((filled-stars))
    (dolist (star-string starlist)
      (push (tlt-nd-fill-star number-of-hyps star-string) filled-stars))
    (reverse filled-stars)))

(defun tlt-nd-find-rule (rulestring)
  "Return that plist in `tlt-nd-proofrules' one of whose :names is RULESTRING, or an error message."
  (let ((right-rule)
        (rulenum (length tlt-nd-proof-rules))
        (counter 1))
    (while (<= counter rulenum)
      (let* ((rulevar (nth (1- counter) tlt-nd-proof-rules))
             (rule (eval rulevar)) ; eval necessary because rulevar is just a sign
             (rulenames (plist-get rule :names)))
        (if (member rulestring rulenames)
            (progn
              (setq right-rule rule)
              (setq counter (1+ rulenum))) ; set the counter so high that the while-loop stops
          (setq counter (1+ counter)))))
    (if right-rule right-rule
      (error "No rule called \"%s\" within tlt-nd-proof-rules" rulestring))))

(defun tlt-nd-get-stars-from-refs (starcol refnums)
  "Return a list of the starstrings in the rows in REFNUMS,
where STARCOL is the number of the column in which the stars are.
STARCOL is an integer, REFNUMS a list of integers."
  (let ((hyplist))
    (dolist (ref refnums)
      (push (tlt-nd-field starcol ref) hyplist))
    (reverse hyplist)))

(defun tlt-nd-no-stars (current-row star-column number-of-hyps)
  "Set the starfield in current-row CURRENT-ROW and column STAR-COLUMN to be a string of NUMBER-OF-HYPS occurrences of '-'.
CURRENT-ROW, STAR-COLUMN and NUMBER-OF-HYPS are integers.

    Meant to be used for deduction rules which do not need a hypothesis."
  (let ((stars-right (make-string number-of-hyps ?-)))
    (tlt-nd-set-field star-column current-row stars-right)
    stars-right))

(defun tlt-nd-new-star (star-column current-row number-of-hyps hyps-until-current-row)
  "Set the starfield in current-row CURRENT-ROW and column STAR-COLUMN to a string of NUMBER-OF-HYPS characters, consisting of occurrences of '-', except for position NUMBER-OF-HYPS, which is '+'.
CURRENT-ROW, STAR-COLUMN, HYPS-UNTIL-CURRENT-ROW and NUMBER-OF-HYPS are integers."
  (let ((stars-right  (tlt-nd-fill-star number-of-hyps (format "%s+" (make-string hyps-until-current-row ?-)))))
    (tlt-nd-set-field star-column current-row stars-right)
    stars-right))

(defun tlt-nd-adopt-stars (star-column current-row reference-list)
  "Set the star field located in the STAR-COLUMN of the CURRENT-ROW to be the same as the first item in REFERENCE-LIST.
CURRENT-ROW and STAR-COLUMN are integers, REFERENCE-LIST is a list of integers."
  (let ((right-star (tlt-nd-field star-column (car reference-list))))
    (tlt-nd-set-field star-column current-row right-star)
    right-star))

(defun tlt-nd-unite (number-of-hyps hyplist)
  "Return the union of all hypotheses in HYPLIST, based on the NUMBER-OF-HYPS.
          Assumes that all hypotheses in HYPLIST have the same length, whitespace removed."
  (let* ((right-star  (make-string number-of-hyps ?-))) ; e.g. " +++"
    (dolist (star-string hyplist)
      (let ((position 1))
        (while (<= position (length star-string))
          (let* ((str (tlt-nd-nth-char position star-string)))
            (when (equal str "+")
              (setq right-star
                    (ensure-star position right-star))))
          (setq position (1+ position)))))
    right-star))

(defun tlt-nd-unite-from-reflist (star-column current-row number-of-hyps reference-list)
  "Set the string in STAR-COLUMN and CURRENT-ROW such that it is the
          intersection of all hypotheses to which REFERENCE-LIST points.
          Assumes that all hypotheses have the same length."
  (let* ((hyplist (tlt-nd-fill-stars number-of-hyps (tlt-nd-get-stars-from-refs star-column reference-list))) ; e.g. ("+--" "++-")
         (union (tlt-nd-unite number-of-hyps hyplist)))
    (tlt-nd-set-field star-column current-row union)))

(defun subtract-from-star (minuend subtrahend)
  "Return the result of calculating the difference between MINUEND and SUBTRAHEND.

With 'calculating the difference', the following is meant:

      + AND + → -
      + AND - → +
      - AND + → +
      - AND - → -

Works like an exclusive OR-statement (if + is interpreted as t and - as nil). Thus, to be exact, there is no minuend or substrahend, since the function is commutative."
  (let ((difference minuend)
        (position 1))
    (while (<= position (length minuend))
      (let* ((symbol_minuend (tlt-nd-nth-char position minuend))
             (symbol_subtrahend (tlt-nd-nth-char position subtrahend)))
        (cond ((and (equal symbol_minuend "+") (equal symbol_subtrahend "+")) ; both +
               (setq difference (ensure-no-star position difference)))
              ((or (and (equal symbol_minuend "+") (equal symbol_subtrahend "-"))
                   (and (equal symbol_minuend "-") (equal symbol_subtrahend "+")))
               (setq difference (ensure-star position difference)))
              ((and (equal symbol_minuend "-") (equal symbol_subtrahend "-"))
               (setq difference (ensure-no-star position difference))))
        (setq position (1+ position))))
    difference))

(defun tlt-nd-subtract (hyplist)
  "Return the result of successively subtracting the cdr of HYPLIST from the car of HYPLIST."
  (let ((subtrahends (cdr hyplist))
        (difference (car hyplist)))
    (dolist (subs subtrahends)
      (setq difference (subtract-from-star difference subs)))
    difference))

(defun tlt-nd-subtract-from-reflist (star-column current-row number-of-hyps reference-list)
  "Set the string in STAR-COLUMN and CURRENT-ROW such that it is the
          intersection of all hypotheses to which REFERENCE-LIST points.
          Assumes that all hypotheses have the same length."
  (let* ((hyplist (tlt-nd-fill-stars number-of-hyps (tlt-nd-get-stars-from-refs star-column reference-list))) ; e.g. ("+--" "++-")
         (difference (tlt-nd-subtract hyplist)))
    (tlt-nd-set-field star-column current-row difference)))

(defun tlt-nd-apply-rules ()
  "Apply rules."
  (let ((rowstotal (1- (length tlt-nd-table))) ; total number of rows
        (current-row 1)                        ; row-counter
        (rule-column (car tlt-nd-rule))        ; in which column the rule column is
        (ref-column (car tlt-nd-reference))    ; in which column the reference column is
        (star-column (car tlt-nd-stars))       ; in which column the star column is
        (number-of-hyps (tlt-nd-count-hyps)))  ; the number of hypotheses

    (while (<= current-row rowstotal)      ; as long as we are at one of the proof's rows,
      (let* ((rule-string (tlt-nd-field rule-column current-row)) ; get name of rule from rule-string
             (proofrule (tlt-nd-find-rule rule-string))           ; find property list of rule with information
             (hyps-until-current-row (tlt-nd-count-hyps-until current-row)))

        ;; If our star function is NOT dependent on hypotheses, set  ;;
        ;; the reference column to be empty and set the hypothesis   ;;
        ;; field to as many minuses as there are hypotheses          ;;

        ;; If our star function IS dependent on hypotheses, get the
        ;; reference field of our row, extract the references, and
        ;; check whether the number of references matches the number
        ;; of references that our proof rule expects. If not, return
        ;; an error.

        (let* ((reference-string (gnus-strip-whitespace (tlt-nd-field ref-column current-row))) ; reference-string
               (reference-list-raw (if (equal reference-string "-") nil (split-string reference-string ","))) ; return nil if our reference list is empty so that the condition below goes through
               (reference-list (mapcar #'string-to-number reference-list-raw))
      (number-of-refs (plist-get proofrule :refs)))                 ; reference list with actual numbers, e.g. '(1 2)

          (if (not (or (equal number-of-refs 'n)
                   (equal (length reference-list) number-of-refs)))
              (error "Proofrule %s needs %d references, but %d provided!"
                     rule-string (plist-get proofrule :refs) (length reference-list))

            ;; If the number of references is correct, though, find the
            ;; star-function's arguments and apply the rule ;;

            (let* ((star-function (plist-get proofrule :star-function))
                   (argument-list (mapcar 'symbol-value (help-function-arglist star-function))))
              (apply star-function argument-list)))))

      (setq current-row (1+ current-row)))))

(defun tlt-nd-make-linestring (line)
  "Return the table string of LINE. If LINE is 1, add an h-line-string."
  (if (= line 1)
      (concat "| "
              (string-join (tlt-nd-line line) "|") "|\n|---")
    (concat "| "
            (string-join (tlt-nd-line line) "|") "|")))

(defun tlt-nd-make-table ()
  "Create a table from the data stored in `tlt-nd-table'."
  (let ((line 1)
        (length (length tlt-nd-table))
        (table ""))
    (while (<= line length)
      (setq table
            (concat table (tlt-nd-make-linestring line) "\n"))
      (setq line (1+ line)))
    table))

(defun tlt-nd-insert-table ()
  "Insert the table stored in `tlt-nd-table' at point."
  (save-excursion
    (insert (tlt-nd-make-table))
    (org-table-align)))

(defun tlt-nd-replace-table ()
  "Delete the table at point and replace it with the table stored in `tlt-nd-table'."
  (let ((marker (point))
        (beg (org-table-begin))
        (end (org-table-end)))
    (delete-region beg end)
    (tlt-nd-insert-table)
    (goto-char marker)
    (re-search-forward "[^|]*" )
    (skip-chars-backward "\s\t"))
      (message "Table updated"))
